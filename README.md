Ktor-Docs [![JitPack](https://jitpack.io/v/dev.horrific.code.james/ktordocs.svg)](https://jitpack.io/#dev.horrific.code.james/ktordocs) [![Discord](https://img.shields.io/discord/514625116706177035.svg?logo=discord&colorB=7289da)](https://discord.jfenn.me/)
=====

This library automatically generates REST API documentation for Ktor/JVM projects and hosts it under a specified URL.

## Screenshots

| Endpoint | Auth Info |
|----------|-----------|
| ![img](./.meta/images/exampleEndpoint.png) | ![img](.meta/images/exampleAuthModal.png) |

## Installation

The `:docs` module is published on [JitPack](https://jitpack.io/), which you can add to your project by copying the following to your root build.gradle at the end of "repositories".

```groovy
allprojects {
  repositories {
    ...
    maven { url 'https://jitpack.io' }
  }
}
```

To add the dependency to a module, copy this line into your program's build.gradle file.

```groovy
implementation "dev.horrific.code.james:ktordocs:$ktordocs_version"
```

## Usage

First, you must install the ktor feature - replacing the default configuration with your project's values.

```kotlin
install(RestDocsFeature) {
    baseUrl = "https://example.com"
    title = "Example API"
    desc = "Basic documentation generated for testing & demo purposes."
}
```

Documentation can then be specified in any endpoint/request handler by invoking the `docs()` function at the start of its callback. (this function will not do anything during normal requests, and should not have any effect on performance)

```kotlin
get("/api/v1/example") {
    docs {
        title = "Hello World"
        desc = "Says hello to the world."
    }

    call.respondText("Hello world!")
}
```

Finally, choose a location to host the documentation at using `restDocumentation(path)`

```kotlin
route("/api") {
    restDocumentation("/docs")
}
```

### Endpoint Parameters

The library will automatically generate a list of URL parameters by parsing the endpoint URL - however, more information can be specified using the `param(...)` function...

```kotlin
get("/api/user/{username}") {
    docs {
        param("username") { // a URL property
            type = "SHA1 hash"
            desc = "A unique identifier of a user."
        }
        param("fetchAll") { // a query parameter (/?fetchAll=true)
            type = "boolean"
            desc = "Whether to fetch all user information from external sources."
            location = ParameterInfo.In.Query
        }
    }

    // ...
}
```

### Endpoint Responses

Some endpoints may return different information depending on the response status - such as an error handler or authentication state. This can be specified using the `responds()` function.

```kotlin
get("/api/user/{username}") {
    docs {
        responds { // default response behavior (200 OK)
            desc = "If the user has been found."

            // this is a handy function to set the `example` property
            // if your response uses a JSON serializer
            exampleJson(UserInfo::class)
        }
        responds(HttpStatusCode.NotFound) {
            desc = "If the user doesn't exist."
            example = """{ "message": "Unable to locate the requested user." }"""
        }
    }

    // ...
}
```

### Links

Endpoints can reference URL links as supporting material, which are provided in a list on the documentation page.

```kotlin
get("/api/user/{username}") {
    docs {
        reference("https://example.com")
        reference("https://example.com", "Example Link")
    }

    // ...
}
```

## Development

The project uses Gradle builds for... err... most things. `./gradlew :{module}:test` and `./gradle :{module}:run` are fairly universal.

## License

```text
Copyright (c) 2020 James Fenn

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at https://mozilla.org/MPL/2.0/.
```
